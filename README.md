# Computer Science

> Just some computer science concepts that I feel the need to write down.
> Actually this is a bit of a misnomer, as I'm going to be using this as a general glossary.

- [ ] Read [Parsing timeline](https://jeffreykegler.github.io/personal/timeline_v3)
- [ ] Read [The Various Kinds of IO - Blocking, Non-blocking, Multiplexed and Async](https://www.rubberducking.com/2018/05/the-various-kinds-of-io-blocking-non.html)

blog.acolyer.org

## Homoiconicity

- [Wikipedia](https://en.wikipedia.org/wiki/Homoiconicity)

Essentially *code is data, data is code* I guess.

## GC - Garbage Collector

## TLAB - Thread Local Allocation Buffer

Relates to GCs.

## TLC

What is this? Relates to GC.

## SBCL - Steel Bank Common Lisp

Has generational GC.

## UB - Undefined Behavior
